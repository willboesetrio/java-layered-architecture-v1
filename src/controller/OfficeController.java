package controller;

import service.OfficeService;

public class OfficeController {

  private OfficeService officeService;

  public void setOfficeService(OfficeService officeService) {
    this.officeService = officeService;
  }

  public void start() {
    officeService.getOffices();
  }
}
