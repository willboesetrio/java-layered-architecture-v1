package controller;

import service.EmployeeService;

public class EmployeeController {

  private EmployeeService employeeService;

  public void setEmployeeService(EmployeeService employeeService) {
    this.employeeService = employeeService;
  }

  public void start() {
    employeeService.getEmployees();
  }
}
