package service;

import dao.OfficeDao;

public class OfficeServiceImpl implements OfficeService {
  private OfficeDao officeDao;

  public void setOfficeDao(OfficeDao officeDao) {
    this.officeDao = officeDao;
  }

  public void getOffices() { officeDao.getOffices(); }
}