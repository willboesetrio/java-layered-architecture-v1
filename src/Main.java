import controller.EmployeeController;
import dao.EmployeeCsvData;
import dao.EmployeeXmlData;
import service.EmployeeServiceImpl;
import controller.OfficeController;
import service.OfficeServiceImpl;
import dao.OfficeData;

public class Main {

  public static void main(String[] args) {
  // Get employee 'data'
    EmployeeController employeeController = new EmployeeController();
    EmployeeServiceImpl employeeServiceImpl = new EmployeeServiceImpl();
    EmployeeCsvData employeeCsvData = new EmployeeCsvData();
    EmployeeXmlData employeeXmlData = new EmployeeXmlData();

    employeeController.setEmployeeService(employeeServiceImpl);

    employeeServiceImpl.setEmployeeDao(employeeCsvData);
    employeeController.start();

    employeeServiceImpl.setEmployeeDao(employeeXmlData);
    employeeController.start();

    // Get office 'data'
    OfficeController officeController = new OfficeController();
    OfficeServiceImpl officeServiceImpl = new OfficeServiceImpl();
    OfficeData officeData = new OfficeData();
    officeController.setOfficeService(officeServiceImpl);

    officeServiceImpl.setOfficeDao(officeData);
    officeController.start();


  }
}