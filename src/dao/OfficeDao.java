package dao;

import entity.Office;
import java.util.List;

public interface OfficeDao {
  List<Office> getOffices();
}